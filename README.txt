UBERCART NETREGISTRY PAYMENT GATEWAY
----------------------------------------
This module adds Netregistry Payment Gateway to Ubercart.

INSTALLATION
----------------------------------------
1. Copy the module as normal.
2. Enable the module from the module administration page.
3. Configure the module (see "Configuration" below).

CONFIGURATION
----------------------------------------
Enable credit card as payment method and configure the module at 
admin/store/settings/payment/method/credit.

CONTACT
----------------------------------------
The current maintainer is Ashish Upadhayay <contact@ashish.com.au>

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the project issue queue.
